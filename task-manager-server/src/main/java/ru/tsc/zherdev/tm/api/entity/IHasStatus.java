package ru.tsc.zherdev.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(@NotNull final Status status);

}
