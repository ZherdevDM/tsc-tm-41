package ru.tsc.zherdev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Delete("DELETE FROM \"task-manager\".\"APP_USER\" WHERE id = #{user.id}")
    void remove(@NotNull @Param("user") User user);

    @Delete("DELETE FROM \"task-manager\".\"APP_USER\"")
    void clear();

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"APP_USER\"")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"APP_USER\" WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"APP_USER\" LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findByIndex(@Param("index") int index);

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"APP_USER\" WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findUserByEmail(@Param("email") String email);

    @Delete("DELETE FROM \"task-manager\".\"APP_USER\" WHERE id = #{id}")
    void removeUserById(@NotNull @Param("id") String id);

    @Insert("INSERT INTO users" +
            " (id, login, password_hash, email, role, first_name, last_name, middle_name, locked)" +
            " VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.role}," +
            " #{user.firstName}, #{user.lastName}, #{user.middleName}, #{user.locked});")
    void add(@NotNull @Param("user") User user);

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"APP_USER\" WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findUserByLogin(@NotNull @Param("login") String login);

    @Delete("DELETE FROM \"task-manager\".\"APP_USER\" WHERE login = #{login}")
    void removeUserByLogin(@NotNull @Param("login") String login);

    @Update("UPDATE users SET password_hash = #{passwordHash} WHERE id = #{id}")
    void setPassword(
            @NotNull @Param("id") String id,
            @NotNull @Param("passwordHash") String passwordHash
    );

    @Update("UPDATE users SET role = #{role} WHERE id = #{id}")
    void setRole(
            @NotNull @Param("id") String id,
            @NotNull @Param("role") Role role
    );

    @Update("UPDATE users" +
            " SET last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName}, " +
            "login = #{login}, password_hash = #{passwordHash}, user_role = #{role}, email = #{email}" +
            " WHERE id = #{id}")
    void update(
            @NotNull @Param("id") String id,
            @NotNull @Param("login") String login,
            @NotNull @Param("passwordHash") String passwordHash,
            @NotNull @Param("email") String email,
            @NotNull @Param("role") Role role,
            @NotNull @Param("lastName") String lastName,
            @NotNull @Param("firstName") String firstName,
            @NotNull @Param("middleName") String middleName
    );

    @Update("UPDATE users SET locked = TRUE WHERE login = #{login}")
    void lockUserByLogin(@NotNull @Param("login") String login);

    @Update("UPDATE users SET locked = FALSE WHERE login = #{login}")
    void unLockUserByLogin(@NotNull @Param("login") String login);

}