package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    protected IServiceLocator serviceLocator;

    protected AbstractEndpoint(@Nullable final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
