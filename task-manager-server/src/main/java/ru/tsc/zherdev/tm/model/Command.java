package ru.tsc.zherdev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@AllArgsConstructor
public class Command {

    @NotNull
    private final String name;

    @Nullable
    private final String argument;

    @NotNull
    private final String description;

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        result += "- " + description;
        return result;
    }

}
