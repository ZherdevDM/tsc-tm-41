package ru.tsc.zherdev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.empty.EmptyTaskIdException;
import ru.tsc.zherdev.tm.exception.entity.EmptyComparatorException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractOwnerBusinessService<Task> implements IOwnerBusinessService<Task> {

    public TaskService(@NotNull final IDataConnectionService connectionService,
                       @NotNull final ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ITaskRepository.class);
    }

    @Override
    @SneakyThrows
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable String description
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        if (description == null || description.isEmpty()) description = "";
        return new Task(userId, name, description);
    }


    @Override
    public @NotNull Task add(@Nullable String userId, @Nullable Task task) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (task == null) {
                logService.error(new TaskNotFoundException());
                throw new TaskNotFoundException();
            }
            taskRepository.add(task);
            session.commit();
            task = findById(userId, task.getId());
            if (task == null) throw new TaskNotFoundException();
            return task;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void addAll(@NotNull final List<Task> taskList) throws SQLException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskList.forEach(taskRepository::add);
            session.commit();
        }
    }

    @Override
    public void remove(@Nullable String userId, @Nullable Task task) throws SQLException {
        if (task == null) throw new EmptyTaskIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @NotNull final String taskId = task.getId();
            if (taskRepository.findByUserIdAndTaskId(userId, taskId) == null)
                throw new TaskNotFoundException();
            taskRepository.remove(userId, task);
            session.commit();
        }
    }

    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) throws SQLException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Task task = findByName(userId, name);
        remove(userId, task);
    }

    @Override
    public void removeById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Task task = findById(userId, id);
        remove(userId, task);
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Task task = findByIndex(userId, index);
        remove(userId, task);
    }

    @Override
    public void clear(@Nullable String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.clearById(userId);
            session.commit();
        }
    }

    @Override
    public @NotNull Integer getSize(@Nullable String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            List<Task> taskList = taskRepository.findAllByUserId(userId);
            if (taskList == null) throw new TaskNotFoundException();
            return taskList.size();
        }
    }

    @Override
    public @Nullable Task findByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final Task task = taskRepository.findByName(userId, name);
            return task;
        }
    }

    @Override
    public @NotNull Task findById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final Task task = taskRepository.findByUserIdAndTaskId(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    public @NotNull Task findByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    public boolean existsByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final Task task = taskRepository.findByIndex(userId, index);
            return taskRepository.existsById(userId, task.getId());
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            return taskRepository.existsById(userId, id);
        }
    }

    @Override
    public @Nullable Task startByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishByName(userId, name, Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return taskRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Task startById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishById(userId, id, Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return taskRepository.findByUserIdAndTaskId(userId, id);
        }
    }

    @Override
    public @Nullable Task startByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            final @Nullable Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.finishByIndex(userId, task.getId(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return taskRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @Nullable Task finishByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishByName(userId, name, Status.COMPLETE.getDisplayName());
            session.commit();
            return taskRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Task finishById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishById(userId, id, Status.COMPLETE.getDisplayName());
            session.commit();
            return taskRepository.findByUserIdAndTaskId(userId, id);
        }
    }

    @Override
    public @Nullable Task finishByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            final @Nullable Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.finishByIndex(userId, task.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            return taskRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @Nullable Task changeStatusByName(@Nullable String userId, @NotNull String name, @NotNull Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.updateStatusByName(userId, name, status.toString());
            session.commit();
            return taskRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Task changeStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.finishById(userId, id, status.getDisplayName());
            session.commit();
            return taskRepository.findByUserIdAndTaskId(userId, id);
        }
    }

    @Override
    public @Nullable Task changeStatusByIndex(
            @Nullable String userId,
            final Integer index,
            @NotNull Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            final @Nullable Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.finishByIndex(userId, task.getId(), status.getDisplayName());
            session.commit();
            return taskRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @NotNull List<Task> findAll(@Nullable String userId) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final List<Task> tasks = taskRepository.findAllByUserId(userId);
            if (tasks == null) throw new TaskNotFoundException();
            return tasks;
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Task> comparator
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(comparator).orElseThrow(EmptyComparatorException::new);
        @Nullable final List<Task> tasks = findAll(userId);
        tasks.sort(comparator);
        return tasks;
    }


    @Override
    public @Nullable Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTaskIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            taskRepository.updateById(userId, id, name, description);
            session.commit();
            return taskRepository.findByUserIdAndTaskId(userId, id);
        }
    }

    @Override
    public @Nullable Task updateByIndex(@Nullable final String userId,
                                        final Integer index,
                                        @NotNull final String name,
                                        @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new EmptyTaskIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable final Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.updateByIndex(userId, task.getId(), name, description);
            session.commit();
            return taskRepository.findByIndex(userId, index);
        }
    }

}
