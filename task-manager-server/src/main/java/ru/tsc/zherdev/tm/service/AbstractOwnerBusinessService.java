package ru.tsc.zherdev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.empty.EmptyIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyIndexException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.empty.EmptyStatusException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public abstract class AbstractOwnerBusinessService<E extends AbstractOwnerBusinessEntity> extends AbstractService<E>
        implements IOwnerBusinessService<E> {

    public AbstractOwnerBusinessService(@NotNull IDataConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

}
