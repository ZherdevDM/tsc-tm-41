package ru.tsc.zherdev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDataConnectionService {

    @NotNull SqlSessionFactory getSqlSessionFactory();

    @NotNull SqlSession getSqlSession();

    @NotNull Connection getConnection() throws SQLException;

}
