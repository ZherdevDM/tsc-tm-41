package ru.tsc.zherdev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerBusinessEntity extends AbstractBusinessEntity {

    @Nullable
    @Column(name = "user_id")
    protected String userId = null;

    @NotNull
    @Override
    public String toString() {
        return super.toString() + " | USER ID: " + userId + "; ";
    }

}
