package ru.tsc.zherdev.tm.api.service;

import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

public interface IOwnerBusinessService<E extends AbstractOwnerBusinessEntity> extends IOwnerBusinessRepository<E> {

}