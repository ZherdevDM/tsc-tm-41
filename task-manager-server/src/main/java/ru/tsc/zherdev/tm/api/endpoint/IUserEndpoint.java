package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;

public interface IUserEndpoint extends PublishedEndpoint {

    @WebMethod
    @Nullable User getUserSelfByUserId(
            @WebParam(name = "session") @Nullable Session session
    ) throws SQLException;

    @WebMethod
    @NotNull User setPasswordUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password
    ) throws SQLException;

    @WebMethod
    @Nullable User updateUserByUserId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException;

    @WebMethod
    @Nullable User updateUserByUserLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException;

}
