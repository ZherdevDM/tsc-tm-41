package ru.tsc.zherdev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository{

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\" WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\" WHERE user_id = #{userId} AND id = #{id};")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByUserIdAndProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\" where user_id = #{userId} AND id = #{id}")
    void removeByUserIdAndProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Insert("INSERT INTO \"task-manager\".\"PROJECT\"" +
            " (id, user_id, name, description, status, create_date, start_date)" +
            " VALUES (#{project.id}, #{project.userId}, #{project.name}, #{project.description}," +
            " #{project.status}, #{project.createDate}, #{project.startDate})")
    void add(@NotNull @Param("project") Project project);

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\"" +
            " WHERE user_id = #{userId} AND name = #{name}")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\"" +
            " WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findByIndex(
            @NotNull @Param("userId") String userId,
            @Param("index") int index
    );

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\"" +
            "WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\"" +
            " SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name};")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE \"task-manager\".\"PROJECT\" " +
            "SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void updateStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\"")
    void clear();

    @Nullable
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\"")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\" WHERE id = #{id};")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    Project findById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\" where id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Select("SELECT 1 FROM \"task-manager\".\"PROJECT\" " +
            "WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date")
    })
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\" WHERE user_id = #{userId};")
    void clearById(@NotNull @Param("userId") String userId);

}
