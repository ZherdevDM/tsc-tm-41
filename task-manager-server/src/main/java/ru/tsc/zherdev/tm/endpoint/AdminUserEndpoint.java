package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.model.Fail;
import ru.tsc.zherdev.tm.model.Result;
import ru.tsc.zherdev.tm.model.Success;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @NotNull
    public User addUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        if (user == null) throw new UserNotFoundException();
        serviceLocator.getUserService().add(user);
        @Nullable final User addedUser = serviceLocator.getUserService().findUserById(user.getId());
        if (addedUser == null) throw new UserNotFoundException();
        return addedUser;
    }

    @Override
    @WebMethod
    public Result addAllUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "collection") @Nullable final List<User> userList
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        if (userList == null) throw new UserNotFoundException();
        try {
            serviceLocator.getUserService().addAll(userList);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result clearUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().clear();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        if (user == null) throw new UserNotFoundException();
        try {
            serviceLocator.getUserService().removeUserById(user.getId());
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeUserById(id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeUserById(id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeUserByLogin(login);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @NotNull
    public List<User> findAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    @Nullable
    public User findByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findUserById(id);
    }

    @Override
    @WebMethod
    @Nullable
    public Integer getSizeUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findAll().size();
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByEmailUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findUserByEmail(email);
    }

    @Override
    @WebMethod
    @Nullable
    public User lockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().lockUserByLogin(login);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    @Nullable
    public User unLockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().unLockUserByLogin(login);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().add(login, password);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUserExtraEmail(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUserExtraRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "passwordHash") @Nullable final String passwordHash,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "role") @Nullable final Role role,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().update(
                userId, login, passwordHash, email, role, lastName, firstName, middleName
        );
        return serviceLocator.getUserService().findUserById(userId);
    }

    @Override
    @WebMethod
    @Nullable
    public User setRoleUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().setRole(session.getUserId(), role);
        return serviceLocator.getUserService().findUserById(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findUserById(id);
    }

    @Override
    @WebMethod
    public boolean isLoginExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isLoginExists(login);
    }

    @Override
    @WebMethod
    public boolean isEmailExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isEmailExists(email);
    }

}
