package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.model.AbstractEntity;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity is not found.");
    }

    public EntityNotFoundException(AbstractEntity entity) {
        super("Error. " + entity.getClass().toString() + " is not found.");
    }

}
