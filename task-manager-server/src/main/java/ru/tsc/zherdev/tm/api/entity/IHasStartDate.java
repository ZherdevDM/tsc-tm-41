package ru.tsc.zherdev.tm.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    void setStartDate(@Nullable final Date startDate);

}
