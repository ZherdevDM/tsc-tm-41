package ru.tsc.zherdev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.api.service.IUserService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.empty.*;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.exception.user.EmptyHashException;
import ru.tsc.zherdev.tm.exception.user.EmptyUserException;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IDataConnectionService connectionService,
                       @NotNull final IPropertyService propertyService,
                       @NotNull final ILogService logService) {
        super(connectionService, logService);
        this.propertyService = propertyService;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IUserRepository.class);
    }

    public String getPasswordHash(@Nullable final String password) {
        return HashUtil.salt(propertyService, password);
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = getPasswordHash(password);
        @NotNull final User user = new User(login, passwordHash);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
        return user;
    }

    @Override
    public void add(@NotNull User user) throws SQLException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
    }

    @Override
    public void addAll(@NotNull List<User> userList) throws SQLException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userList.forEach(userRepository::add);
            session.commit();
        }
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @NotNull final User user = new User(login, getPasswordHash(password));
        user.setEmail(email);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        @NotNull final User user = new User(login, getPasswordHash(password));
        user.setRole(role);
        add(user);
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable User user = findUserById(userId);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            String passwordHash = getPasswordHash(password);
            userRepository.update(
                    user.getId(), user.getLogin(), passwordHash, user.getEmail(), user.getRole(),
                    user.getLastName(), user.getFirstName(), user.getMiddleName()
            );
            session.commit();
        }
    }

    @Override
    public void setRole(@Nullable final String userId, @Nullable final Role role) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable User user = findUserById(userId);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            userRepository.update(
                    user.getId(), user.getLogin(), user.getPasswordHash(), user.getEmail(), role,
                    user.getLastName(), user.getFirstName(), user.getMiddleName()
            );
            session.commit();
        }
    }

    @Override
    @Nullable
    public User findUserById(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable User user = userRepository.findById(userId);
            return user;
        }
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserNotFoundException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable User user = userRepository.findUserByLogin(login);
            return user;
        }
    }

    @Override
    @Nullable
    public User findUserByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable User user = userRepository.findUserByEmail(email);
            return user;
        }
    }

    @Override
    @NotNull
    public List<User> findAll() throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable List<User> userList = userRepository.findAll();
            if (userList == null) throw new UserNotFoundException();
            return userList;
        }
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.clear();
            session.commit();
        }
    }

    @Override
    public void removeUserById(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.removeUserById(userId);
            session.commit();
        }
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.removeUserByLogin(login);
            session.commit();
        }
    }

    @Override
    @Nullable
    public User userValidate(@Nullable String login, @Nullable String password) {
        @Nullable User user = findUserByLogin(login);
        if (user == null) {
            logService.error(new UserNotFoundException());
            throw new UserNotFoundException();
        }
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final String hash = getPasswordHash(password);
        if (!hash.equals(user.getPasswordHash())) return null;
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login){
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        return findUserByEmail(email) != null;
    }

    @Override
    public void update(@Nullable String id, @Nullable String login, @Nullable String password,
                       @Nullable String email, @Nullable Role role, @Nullable String lastName,
                       @Nullable String firstName, @Nullable String middleName) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        Optional.ofNullable(lastName).orElseThrow(EmptyLastNameException::new);
        Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.ofNullable(middleName).orElseThrow(EmptyMiddleNameException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @Nullable User updatedUser;
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            updatedUser = userRepository.findById(id);
            if (updatedUser == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            final String passwordHash = getPasswordHash(password);
            Optional.ofNullable(passwordHash).orElseThrow(EmptyHashException::new);
            if (!updatedUser.getLogin().equals(login)) updatedUser.setLogin(login);
            if (!updatedUser.getPasswordHash().equals(passwordHash)) updatedUser.setPasswordHash(passwordHash);
            if (!updatedUser.getEmail().equals(email)) updatedUser.setEmail(email);
            if (updatedUser.getRole() != role) updatedUser.setRole(role);
            if (!updatedUser.getLastName().equals(lastName)) updatedUser.setLastName(lastName);
            if (!updatedUser.getFirstName().equals(firstName)) updatedUser.setFirstName(firstName);
            if (!updatedUser.getMiddleName().equals(middleName)) updatedUser.setMiddleName(middleName);
            userRepository.update(updatedUser.getId(), updatedUser.getLogin(),
                    updatedUser.getPasswordHash(), updatedUser.getEmail(), updatedUser.getRole(),
                    updatedUser.getLastName(), updatedUser.getFirstName(), updatedUser.getMiddleName());
            session.commit();
        }
    }

    @Override
    public void updateUserById(@Nullable final String userId, @Nullable final String lastName,
                               @Nullable final String firstName, @Nullable final String middleName,
                               @Nullable final String email) {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        @Nullable final User user = findUserById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        update(userId, user.getLogin(), user.getPasswordHash(), email, user.getRole(), lastName, firstName, middleName);
    }

    @Override
    public void updateUserByLogin(@Nullable final String userId, @Nullable final String login,
                                           @Nullable final String lastName, @Nullable final String firstName,
                                           @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(EmptyUserException::new);
        update(user.getId(), login, user.getPasswordHash(), email, user.getRole(), lastName, firstName, middleName);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login){
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.unLockUserByLogin(login);
            session.commit();
        }
    }

    @Override
    public void unLockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.unLockUserByLogin(login);
            session.commit();
        }
    }

}
