package ru.tsc.zherdev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IService;
import ru.tsc.zherdev.tm.exception.empty.EmptyIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyIndexException;
import ru.tsc.zherdev.tm.exception.entity.EmptyComparatorException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IDataConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

}