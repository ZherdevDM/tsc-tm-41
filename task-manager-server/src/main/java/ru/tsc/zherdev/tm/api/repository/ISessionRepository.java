package ru.tsc.zherdev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO sessions" +
            " (id, login_timestamp, signature, user_id)" +
            " VALUES (#{session.id}, #{session.timestamp}, #{session.signature}, #{session.userId});")
    Session open(@NotNull @Param("session") final Session session);

    @Update("DELETE FROM \"task-manager\".\"PROJECT\" WHERE id = #{session.id};")
    boolean close(@NotNull @Param("session") final Session session);

    @Select("SELECT 1 FROM \"task-manager\".\"PROJECT\" WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "login_timestamp")
    })
    boolean contains(@NotNull @Param("id") final String id);

    @NotNull
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\"")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "login_timestamp")
    })
    List<Session> findAll();

    @NotNull
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\" WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "login_timestamp")
    })
    Session findById(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM \"task-manager\".\"PROJECT\" LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "login_timestamp")
    })
    Session findByIndex(@Param("index") int index);

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\" WHERE id = #{session.id}")
    void remove(@NotNull @Param("session") Session session);

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\"")
    void clear();

    @Delete("DELETE FROM \"task-manager\".\"PROJECT\" WHERE user_id = #{userId}")
    boolean removeByUserId(@NotNull @Param("userId") String id);

}
