package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(@Nullable final String currentUserId);

}
