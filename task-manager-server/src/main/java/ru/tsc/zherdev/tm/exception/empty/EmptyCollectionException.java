package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyCollectionException extends AbstractException {
    public EmptyCollectionException() {
        super("Error. Collection is empty");
    }
}
