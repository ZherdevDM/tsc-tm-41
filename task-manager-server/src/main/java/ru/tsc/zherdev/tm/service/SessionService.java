package ru.tsc.zherdev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.service.*;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.empty.EmptyLoginException;
import ru.tsc.zherdev.tm.exception.empty.EmptySessionException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.util.HashUtil;

import java.sql.SQLException;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
            @NotNull final ILogService logService,
            @NotNull final IDataConnectionService connectionService,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService, logService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    public String getPasswordHash(@Nullable final String password) {
        return HashUtil.salt(propertyService, password);
    }

    @Override
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) throws SQLException {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = userService.findUserByLogin(login);
        if (user == null) return false;
        final String passwordHash = getPasswordHash(password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    public Session login(@Nullable String login, @Nullable String password) throws SQLException {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        if (login == null) {
            logService.error(new EmptyLoginException());
            throw new EmptyLoginException();
        }
        final User user = userService.findUserByLogin(login);
        if (user == null) return null;

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            session = sessionRepository.open(session);
            sqlSession.commit();
        }
        return session;
    }

    @Override
    public @Nullable Session sign(@Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final int iteration = propertyService.getSignatureIteration();
        @NotNull String secret = propertyService.getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(session, secret, iteration);
        session.setSignature(signature);
        return session;

    }

    @Override
    public @Nullable User getUser(@Nullable Session session) throws AccessDeniedException, SQLException {
        if (session == null) return null;

        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return userService.findUserById(userId);
    }

    @Override
    public @Nullable String getUserId(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        return session.getUserId();
    }

    @Override
    public @Nullable List<Session> getListSession(@Nullable Session session) throws AccessDeniedException, SQLException {
        if (session == null) throw new AccessDeniedException();
        validateAdmin(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

    @Override
    public @Nullable Session getSessionById(
            @Nullable final Session session, @Nullable final String sessionId
    ) throws AccessDeniedException, SQLException {
        if (session == null) throw new AccessDeniedException();
        validateAdmin(session);
        if (sessionId == null || sessionId.isEmpty()) throw new EmptySessionException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findById(sessionId);
        }
    }

    @Override
    public @Nullable Session getSessionByIndex(
            @Nullable Session session, final Integer index
    ) throws AccessDeniedException, SQLException {
        if (session == null) throw new AccessDeniedException();
        validateAdmin(session);
        if (index < 0) throw new EmptySessionException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findByIndex(index);
        }
    }

    @Override
    public @Nullable Session open(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            session = sessionRepository.open(session);
            sqlSession.commit();
            return session;
        }
    }

    @Override
    public void close(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            sessionRepository.close(session);
            sqlSession.commit();
        }
    }

    @Override
    public void closeAll(@Nullable Session session) throws AccessDeniedException, SQLException {
        if (session == null) throw new AccessDeniedException();
        validateAdmin(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            sessionRepository.clear();
            sqlSession.commit();
        }
    }

    @SneakyThrows
    @Override
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        if (sessionTemp.getSignature() == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource == null || signatureSource.isEmpty()) throw new AccessDeniedException();
        @Nullable final Session targetSession = sign(sessionTemp);
        if (targetSession == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = targetSession.getSignature();
        if (signatureTarget == null || signatureTarget.isEmpty()) throw new AccessDeniedException();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sign(session);
            if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
            sqlSession.commit();
        }
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findUserById(userId);
        if (user == null) throw new AccessDeniedException();
    }

    @Override
    public void validateAdmin(@Nullable Session session) throws AccessDeniedException, SQLException {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findUserById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!user.getRole().equals(Role.ADMIN)) throw new AccessDeniedException();
    }

    @Override
    public boolean isValid(@Nullable Session session) {
        if (session == null) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findById(session.getId()).getId() != null;
        }
    }

    @Override
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final User user = userService.findUserByLogin(login);
        if (user == null) return;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeByUserId(user.getId());
            sqlSession.commit();
        }
    }

    @Override
    public void signOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeByUserId(userId);
            sqlSession.commit();
        }
    }

}
