package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyUserListException extends AbstractException {
    public EmptyUserListException() {
        super("Error. User list is empty.");
    }
}
