package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException;

    @Nullable
    List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final Comparator<Task> comparator) throws SQLException;

    @Nullable
    Task bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException;

    @Nullable
    Task unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException;

    @Nullable
    void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws SQLException;

}
