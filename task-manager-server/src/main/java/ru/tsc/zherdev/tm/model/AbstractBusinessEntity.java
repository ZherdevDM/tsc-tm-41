package ru.tsc.zherdev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.entity.IWBS;
import ru.tsc.zherdev.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @NotNull
    @Column(name = "name")
    protected String name = "";

    @NotNull
    @Column(name = "description")
    protected String description = "";

    @NotNull
    @Column(name = "status")
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    protected Date startDate;

    @Nullable
    @Column(name = "finish_date")
    protected Date finishDate;

    @NotNull
    @Column(name = "create_date")
    protected Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return super.toString() + " | NAME: " + name +
                " | DESCRIPTION" + description +
                " | STATUS" + status +
                " | FINISH DATE" + finishDate;
    }

}