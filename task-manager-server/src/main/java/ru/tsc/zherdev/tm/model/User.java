package ru.tsc.zherdev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "APP_USER")
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractEntity {

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @NotNull
    @Column
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column
    private String email;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "user_role")
    private Role role = Role.USER;

    @NotNull
    @Column
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return super.toString() + " | LOGIN: " + login +
                " | EMAIL: " + email +
                " | ROLE: " + role +
                " | LOCKED: " + locked;
    }

}
