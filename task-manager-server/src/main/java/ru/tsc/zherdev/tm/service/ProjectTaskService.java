package ru.tsc.zherdev.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IProjectTaskService;
import ru.tsc.zherdev.tm.exception.empty.EmptyProjectIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyTaskIdException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IDataConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    @NotNull
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final List<Task> taskList = taskRepository.findTaskByProjectId(userId, projectId);
            if (taskList == null) throw new SQLExecutionFailedException();
            return taskList;
        }
    }

    @Override
    @Nullable
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable String projectId,
                                          @Nullable Comparator<Task> comparator) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final List<Task> taskList = taskRepository.findTaskByProjectId(userId, projectId);
            if (comparator == null) return taskList;
            if (taskList == null) throw new SQLExecutionFailedException();
            return taskList.stream()
                    .filter(task -> projectId.equals(task.getProjectId()))
                    .sorted(comparator)
                    .collect(Collectors.toList());
        }
    }

    @Override
    @Nullable
    public Task bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        Optional.ofNullable(taskId).orElseThrow(EmptyTaskIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
            taskRepository.bindTaskById(userId, projectId, taskId);
            @Nullable final Task task = taskRepository.findById(taskId);
            session.commit();
            return task;
        }
    }

    @Override
    @Nullable
    public Task unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        Optional.ofNullable(taskId).orElseThrow(EmptyTaskIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
            taskRepository.unbindTaskById(userId, taskId);
            @Nullable final Task task = taskRepository.findById(taskId);
            session.commit();
            return task;
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            findTaskByProjectId(userId, projectId).forEach(o -> {
                taskRepository.removeById(o.getId());
            });
            projectRepository.removeById(projectId);
        }
    }

}
