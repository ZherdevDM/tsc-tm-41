package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@Nullable final String login, @Nullable final String password) throws SQLException;

    void add(@NotNull User user) throws SQLException;

    void addAll(@NotNull List<User> userList) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException;

    void setPassword(@Nullable final String userId, @Nullable final String password);

    void setRole(@Nullable final String userId, @Nullable final Role role);

    @Nullable
    User findUserById(@Nullable final String id) throws SQLException;

    @Nullable User findUserByLogin(@Nullable String login);

    @Nullable User findUserByEmail(@Nullable String email);

    @NotNull
    List<User> findAll() throws AbstractException;

    void clear();

    void removeUserById(@Nullable String userId);

    void removeUserByLogin(@Nullable String login);

    @Nullable User userValidate(@Nullable String login, @Nullable String password);

    boolean isLoginExists(@Nullable final String login) throws SQLException;

    boolean isEmailExists(@Nullable final String email) throws SQLException;

    void update(@Nullable String id, @Nullable String login, @Nullable String password,
                @Nullable String email, @Nullable Role role, @Nullable String lastName,
                @Nullable String firstName, @Nullable String middleName);

    void updateUserById(
            @Nullable final String userId, @Nullable final String email,
            @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName
    );

    void updateUserByLogin(
            @Nullable final String userId, @Nullable final String login,
            @Nullable final String email, @Nullable final String lastName,
            @Nullable final String firstName, @Nullable final String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unLockUserByLogin(@Nullable String login);

}
