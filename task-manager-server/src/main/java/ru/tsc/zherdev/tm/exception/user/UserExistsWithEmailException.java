package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class UserExistsWithEmailException extends AbstractException {

    public UserExistsWithEmailException(final String email) {
        super("Error. Such email '" + email + "' has already used.");
    }

}
