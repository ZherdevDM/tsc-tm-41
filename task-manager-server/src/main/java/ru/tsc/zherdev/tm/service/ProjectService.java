package ru.tsc.zherdev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.empty.EmptyProjectIdException;
import ru.tsc.zherdev.tm.exception.entity.EmptyComparatorException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.model.Project;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public final class ProjectService extends AbstractOwnerBusinessService<Project> implements IOwnerBusinessService<Project> {

    public ProjectService(@NotNull final IDataConnectionService connectionService,
                          @NotNull final ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IProjectRepository.class);
    }

    @Override
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        if (description == null)
            description = "";
        @Nullable final Project project = new Project(userId, name, description);
        return project;
    }

    @Override
    public @NotNull Project add(@Nullable final String userId, @Nullable Project project) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = getRepository(session);
            repository.add(project);
            session.commit();
            project = findById(userId, project.getId());
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public void addAll(@NotNull List<Project> projectList) throws SQLException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            projectList.forEach(repository::add);
            session.commit();
        }
    }

    @Override
    public void remove(@Nullable String userId, @Nullable Project project) throws SQLException {
        if (project == null) throw new EmptyProjectIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String projectId = project.getId();
            if (projectRepository.findByUserIdAndProjectId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByUserIdAndProjectId(userId, projectId);
            session.commit();
        }
    }

    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) throws SQLException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Project project = findByName(userId, name);
        remove(userId, project);
    }

    @Override
    public void removeById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Project project = findById(userId, id);
        remove(userId, project);
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Project project = findByIndex(userId, index);
        remove(userId, project);
    }

    @Override
    public void clear(@Nullable String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            List<Project> projectList = projectRepository.findAllByUserId(userId);
            if (projectList == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            projectList.forEach(project -> taskRepository.removeAllTaskByProjectId(userId, project.getId()));
            projectRepository.clearById(userId);
            session.commit();
        }
    }

    @Override
    public @NotNull Integer getSize(@Nullable String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            List<Project> projectList = projectRepository.findAllByUserId(userId);
            if (projectList == null) throw new ProjectNotFoundException();
            return projectList.size();
        }
    }

    @Override
    public @Nullable Project findByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByName(userId, name);
            return project;
        }
    }

    @Override
    public @NotNull Project findById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public @NotNull Project findByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public boolean existsByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            return projectRepository.existsById(userId, project.getId());
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            return projectRepository.existsById(userId, id);
        }
    }

    @Override
    public @Nullable Project startByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishByName(userId, name, Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return projectRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Project startById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishById(userId, id, Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return projectRepository.findByUserIdAndProjectId(userId, id);
        }
    }

    @Override
    public @Nullable Project startByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            final @Nullable Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.finishByIndex(userId, project.getId(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            return projectRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @Nullable Project finishByName(@Nullable String userId, @NotNull String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishByName(userId, name, Status.COMPLETE.getDisplayName());
            session.commit();
            return projectRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Project finishById(@Nullable String userId, @Nullable String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishById(userId, id, Status.COMPLETE.getDisplayName());
            session.commit();
            return projectRepository.findByUserIdAndProjectId(userId, id);
        }
    }

    @Override
    public @Nullable Project finishByIndex(@Nullable String userId, final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            final @Nullable Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.finishByIndex(userId, project.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            return projectRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @Nullable Project changeStatusByName(@Nullable String userId, @NotNull String name, @NotNull Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.updateStatusByName(userId, name, status.toString());
            session.commit();
            return projectRepository.findByName(userId, name);
        }
    }

    @Override
    public @Nullable Project changeStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishById(userId, id, status.getDisplayName());
            session.commit();
            return projectRepository.findByUserIdAndProjectId(userId, id);
        }
    }

    @Override
    public @Nullable Project changeStatusByIndex(
            @Nullable String userId,
            final Integer index,
            @NotNull Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            final @Nullable Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.finishByIndex(userId, project.getId(), status.getDisplayName());
            session.commit();
            return projectRepository.findByIndex(userId, index);
        }
    }

    @Override
    public @NotNull List<Project> findAll(@Nullable String userId) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final List<Project> projects = projectRepository.findAllByUserId(userId);
            if (projects == null) throw new ProjectNotFoundException();
            return projects;
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Project> comparator
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(comparator).orElseThrow(EmptyComparatorException::new);
        @Nullable final List<Project> projects = findAll(userId);
        projects.sort(comparator);
        return projects;
    }


    @Override
    public @Nullable Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyProjectIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.updateById(userId, id, name, description);
            session.commit();
            return projectRepository.findByUserIdAndProjectId(userId, id);
        }
    }

    @Override
    public @Nullable Project updateByIndex(@Nullable final String userId,
                                           final Integer index,
                                           @NotNull final String name,
                                           @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new EmptyProjectIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            projectRepository.updateByIndex(userId, project.getId(), name, description);
            session.commit();
            return projectRepository.findByIndex(userId, index);
        }
    }

}
