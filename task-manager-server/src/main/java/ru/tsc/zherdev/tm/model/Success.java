package ru.tsc.zherdev.tm.model;

import ru.tsc.zherdev.tm.model.Result;

public class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }
}
