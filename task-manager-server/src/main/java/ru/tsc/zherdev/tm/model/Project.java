package ru.tsc.zherdev.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@NoArgsConstructor
@Table(name = "APP_PROJECT")
@XmlRootElement(name = "Project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project extends AbstractOwnerBusinessEntity {

    public Project(@NotNull final String userId, @NotNull final String name) {
        this.userId = userId;
        this.name = name;
    }

    public Project(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return super.toString();
    }

}
