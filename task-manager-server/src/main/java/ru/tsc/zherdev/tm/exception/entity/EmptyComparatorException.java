package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyComparatorException extends AbstractException {

    public EmptyComparatorException() {
        super("Error. Comparator is empty.");
    }
}
