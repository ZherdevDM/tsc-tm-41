package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyHashException extends AbstractException {

    public EmptyHashException() {
        super("Error. Password hash is empty.");
    }

}
