package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyServiceLocatorException extends AbstractException {

    public EmptyServiceLocatorException() {
        super("Error. No");
    }

}
