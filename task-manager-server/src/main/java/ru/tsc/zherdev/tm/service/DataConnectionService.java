package ru.tsc.zherdev.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

import static org.hibernate.cfg.AvailableSettings.*;

@RequiredArgsConstructor
public class DataConnectionService implements IDataConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ILogService logService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public DataConnectionService(
            @NotNull final IPropertyService propertyService,
            @NotNull final ILogService logService
    ) {
        this.propertyService = propertyService;
        this.logService = logService;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public Connection getConnection() throws SQLException {
        @NotNull final String url = propertyService.getJDBCurl();
        @NotNull final Properties properties = new Properties();
        properties.setProperty("user", propertyService.getJDBCUser());
        properties.setProperty("password", propertyService.getJDBCPassword());
        try(@NotNull final Connection connection = DriverManager.getConnection(url, properties)) {
            connection.setAutoCommit(false);
            return connection;
        }
    }


    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String url = propertyService.getJDBCurl();
        @NotNull final String driver = propertyService.getJDBCDriver();
        @NotNull final Properties properties = new Properties();
        properties.setProperty("user", propertyService.getJDBCUser());
        properties.setProperty("password", propertyService.getJDBCPassword());
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, properties);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, propertyService.getJDBCDriver());
        settings.put(URL, propertyService.getJDBCurl());
        settings.put(USER, propertyService.getJDBCUser());
        settings.put(PASS, propertyService.getJDBCPassword());
        settings.put(DIALECT, propertyService.getHibernateDialect());
        settings.put(SHOW_SQL, propertyService.getHibernateShowSql());
        settings.put(FORMAT_SQL, propertyService.getHibernateFormatSql());
        settings.put(HBM2DDL_AUTO, propertyService.getHibernateHbm2Ddl());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry serviceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(serviceRegistry);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.buildMetadata();
        return metadata.getSessionFactoryBuilder().build();
    }

    private @NotNull List<String> createQueriesFromFile(@Nullable final String path) {
        if (path == null && path.isEmpty()) return Collections.emptyList();
        String queryLine = "";
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> listOfQueries = new ArrayList<>();

        try (
                final FileReader fileReader = new FileReader(path);
                final BufferedReader bufferedReader = new BufferedReader(fileReader)
        ) {
            while ((queryLine = bufferedReader.readLine()) != null) {
                skipComments(stringBuilder, bufferedReader, queryLine);
                stringBuilder.append(queryLine).append(" ");
            }

            final String[] splitQueries = stringBuilder.toString().split(";");

            for (@NotNull final String splitQuery : splitQueries) {
                if (!splitQuery.trim().equals("") && !splitQuery.trim().equals("\t")) {
                    listOfQueries.add(splitQuery);
                }
            }
        } catch (Exception e) {
            logService.error(e);
            logService.debug(e.toString());
            logService.debug(stringBuilder.toString());
        }

        return listOfQueries;
    }

    private void skipComments(
            @NotNull final StringBuilder stringBuilder,
            @NotNull final BufferedReader br,
            @NotNull String queryLine
    ) throws IOException {
        final String[] commentSymbols = new String[]{"#", "--", "/*"};

        for (@NotNull final String ignored : commentSymbols) {
            int indexOfCommentSymbol = queryLine.indexOf(ignored);
            if (indexOfCommentSymbol != -1 && queryLine.startsWith(ignored)) queryLine = "";
            queryLine = queryLine.substring(0, indexOfCommentSymbol - 1);

            if (ignored.equals("/*")) {
                stringBuilder.append(queryLine).append(" ");
                do {
                    queryLine = br.readLine();
                }
                while (queryLine != null && !queryLine.contains("*/"));
                assert queryLine != null;
                indexOfCommentSymbol = queryLine.indexOf("*/");
                if (indexOfCommentSymbol != -1 && queryLine.endsWith("*/")) queryLine = "";
                else
                    queryLine = queryLine.substring(indexOfCommentSymbol + 2, queryLine.length() - 1);
            }
        }
    }

}