package ru.tsc.zherdev.tm.comparator;

import ru.tsc.zherdev.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    private static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate() {
    }

    public static ComparatorByStartDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasStartDate o1, final IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStartDate() == null && o2.getStartDate() == null) return 0;
        if (o1.getStartDate() == null) return 1;
        if (o2.getStartDate() == null) return -1;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }
}
