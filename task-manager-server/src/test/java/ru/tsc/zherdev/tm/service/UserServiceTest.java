package ru.tsc.zherdev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private UserService userService;

    @Nullable
    private User user;

    @Before
    public void before() {
        @NotNull IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            @NotNull final PropertyService propertyService = new PropertyService();
            userService = new UserService(dataConnectionService, propertyService, logService);

            user = new User("login", "passwordHash");
            Assert.assertNotNull(user);
            user.setRole(Role.ADMIN);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());

            userRepository.add(new User("test", "passwordHash", Role.ADMIN));
            user = userRepository.findUserByLogin("test");
            Assert.assertNotNull(user);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("test", user.getLogin());
        }
    }

    @Test
    public void create() throws SQLException {
        Assert.assertNotNull(user);
        @Nullable final User testUser = userService.create(user.getId(), "Test1", "Test1 description");
        Assert.assertNotNull(testUser);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        @Nullable final User userById = userService.findUserById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<User> users = new ArrayList<>();
        users.add(new User("test2", "test2", Role.USER));
        users.add(new User("test2", "test3"));
        userService.addAll(users);
        users = userService.findAll();
        Assert.assertNotEquals(Collections.EMPTY_LIST, users);
        Assert.assertEquals(3, users.size());
    }

    @Test
    public void findAll()  {
        @Nullable List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findAllByUserId() {
        @Nullable List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findUserById() {
        Assert.assertNotNull(user);
        @Nullable final User userById = userService.findUserById(user.getId());
        Assert.assertNotNull(userById);
    }

    @Test
    public void existsById(){Assert.assertNotNull(user);
        @Nullable final User userById = userService.findUserById(user.getId());
        Assert.assertNotNull(userById);
    }

    @Test
    public void removeById() throws SQLException {
        Assert.assertNotNull(user);
        userService.removeUserById(user.getId());
        Assert.assertNull(userService.findUserById(user.getId()));
        userService.add(user);
    }

    @Test
    public void update() {
        @NotNull final String email = "email@g.com";
        @NotNull final String lastName = "Smith";
        @NotNull final String firstName = "John";
        @NotNull final String middleName = "Billy";
        Assert.assertNotNull(user);
        userService.update(user.getId(), user.getLogin(),
                user.getPasswordHash(), email, Role.ADMIN, lastName, firstName, middleName);
        @Nullable User updatedUser = userService.findUserById(user.getId());
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(user.getId(), updatedUser.getId());
        Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), updatedUser.getPasswordHash());
        Assert.assertEquals(email, updatedUser.getEmail());
        Assert.assertEquals(Role.ADMIN, updatedUser.getRole());
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertNotNull(user);
        userService.lockUserByLogin(user.getLogin());
        @Nullable User userByLogin = userService.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(true, userByLogin.getLocked());
        userService.unLockUserByLogin(user.getLogin());
    }

    @Test
    public void unLockUserByLogin() {
        Assert.assertNotNull(user);
        userService.unLockUserByLogin(user.getLogin());
        @Nullable User userByLogin = userService.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(false, userByLogin.getLocked());
    }

}