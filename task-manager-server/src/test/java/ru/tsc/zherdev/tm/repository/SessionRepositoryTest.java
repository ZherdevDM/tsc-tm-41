package ru.tsc.zherdev.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.util.Collections;
import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private Session session;

    @Nullable User user;

    @NotNull
    public ISessionRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ISessionRepository.class);
    }

    @Before
    public void before() {

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        session = new Session();
        session.setUserId(user.getId());
        Assert.assertNotNull(session.getId());
    }

    @Test
    public void add() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            @Nullable final Session sessionById = sessionRepository.findById(session.getId());
            Assert.assertNotNull(sessionById);
            Assert.assertEquals(session, sessionById);
        }
    }

    @Test
    public void findAll() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            @Nullable List<Session> sessions = sessionRepository.findAll();
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
        }
    }

    @Test
    public void clear() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            @Nullable List<Session> sessions = sessionRepository.findAll();
            Assert.assertNotNull(sessions);
            sessionRepository.clear();
            sqlSession.commit();
            Assert.assertEquals(Collections.EMPTY_LIST, sessionRepository.findAll());
            sessions.forEach(sessionRepository::open);
            sqlSession.commit();
        }
    }

    @Test
    public void remove() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            sessionRepository.remove(session);
            sqlSession.commit();
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            sessionRepository.open(session);
            sqlSession.commit();
        }
    }

    @Test
    public void removeById() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            sessionRepository.removeByUserId(session.getId());
            sqlSession.commit();
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            sessionRepository.open(session);
            sqlSession.commit();
        }
    }

    @Test
    public void findById() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            @Nullable final Session sessionById = sessionRepository.findById(session.getId());
            Assert.assertNotNull(sessionById);
        }
    }

    @Test
    public void findByIndex() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            @Nullable final Session sessionByIndex = sessionRepository.findByIndex(0);
            Assert.assertNotNull(sessionByIndex);
        }
    }

    @Test
    public void open() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            sessionRepository.open(session);
            sqlSession.commit();
            @Nullable Session openedSession = sessionRepository.findById(session.getId());
            Assert.assertNotNull(openedSession);
            Assert.assertEquals(session.getId(), openedSession.getId());
        }
    }

    @Test
    public void contains() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            @Nullable Session nonContainedSession = new Session();
            Assert.assertNotNull(nonContainedSession);
            boolean isContains = sessionRepository.contains(nonContainedSession.getId());
            Assert.assertFalse(isContains);
            isContains = sessionRepository.contains(session.getId());
            Assert.assertTrue(isContains);
        }
    }

    @Test
    public void removeByUserId() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            Assert.assertNotNull(session);
            Assert.assertNotNull(session.getUserId());
            boolean isRemoved = sessionRepository.removeByUserId(session.getUserId());
            sqlSession.commit();
            Assert.assertTrue(isRemoved);
            @Nullable Session removedSession = sessionRepository.findById(session.getId());
            Assert.assertNull(removedSession.getUserId());
        }
    }

    @Test
    public void close() {
        try (@NotNull final SqlSession sqlSession = dataConnectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            @Nullable final Session closableSession = new Session();
            Assert.assertNotNull(closableSession);
            @Nullable final Session openedSession = sessionRepository.open(closableSession);
            sqlSession.commit();
            Assert.assertNotNull(openedSession);
            Assert.assertEquals(closableSession.getId(), openedSession.getId());
            boolean isClosed = sessionRepository.close(closableSession);
            sqlSession.commit();
            Assert.assertTrue(isClosed);
        }
    }

}
