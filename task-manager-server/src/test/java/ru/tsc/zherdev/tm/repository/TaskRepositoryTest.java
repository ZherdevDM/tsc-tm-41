package ru.tsc.zherdev.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @Nullable
    private User user;

    @NotNull
    public ITaskRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ITaskRepository.class);
    }

    @Before
    public void before() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            user = new User("login", "passwordHash");
            Assert.assertNotNull(user);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());

            project = new Project(user.getId(), "test", "test description");
            project.setName("project");
            project.setDescription("project description");
            project.setUserId(user.getId());
            Assert.assertNotNull(project);


            taskRepository.add(new Task(user.getId(), "test", "test description"));
            task = taskRepository.findByName(user.getId(), "test");
            Assert.assertNotNull(task);
            Assert.assertNotNull(task.getId());
            Assert.assertNotNull(task.getName());
            Assert.assertEquals("test", task.getName());
            taskRepository.bindTaskById(user.getId(), project.getId(), task.getId());
        }
    }

    @Test
    public void add() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            if (task == null) Assert.fail();
            @Nullable final Task taskById = taskRepository.findById(task.getId());
            Assert.assertNotNull(taskById);
            Assert.assertEquals(task, taskById);
        }
    }

    @Test
    public void addAll() {
        @Nullable List<Task> tasks = new ArrayList<>();
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            if (user == null) Assert.fail();
            tasks.add(new Task(user.getId(), "test2", "test2 description"));
            tasks.add(new Task(user.getId(), "test3", "test3 description"));
            tasks.forEach(taskRepository::add);
            tasks = taskRepository.findAll();
            Assert.assertNotEquals(Collections.EMPTY_LIST, tasks);
            Assert.assertNotNull(tasks);
            Assert.assertEquals(2, tasks.size());
        }
    }

    @Test
    public void findAll() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            @Nullable List<Task> tasks = taskRepository.findAll();
            Assert.assertNotNull(tasks);
            Assert.assertEquals(1, tasks.size());
        }
    }

    @Test
    public void findAllByUserId() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable List<Task> tasks = taskRepository.findAllByUserId(user.getId());
            Assert.assertNotNull(tasks);
            Assert.assertEquals(1, tasks.size());
        }
    }

    @Test
    public void findByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            @Nullable final Task taskByName = taskRepository.findByName(user.getId(), task.getName());
            Assert.assertNotNull(taskByName);
        }
    }

    @Test
    public void findById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            @Nullable final Task taskById = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(taskById);
        }
    }

    @Test
    public void findByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final Task taskByIndex = taskRepository.findByIndex(user.getId(), 0);
            Assert.assertNotNull(taskByIndex);
        }
    }

    @Test
    public void existsByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final Task taskByIndex = taskRepository.findByIndex(user.getId(), 0);
            Assert.assertNotNull(taskByIndex);
        }
    }

    @Test
    public void existsById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            @Nullable final Task taskById = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(taskById);
        }
    }

    @Test
    public void updateById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.updateById(
                    user.getId(), task.getId(), "updated test", "updated test description"
            );
            session.commit();
            @Nullable final Task updatedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals("updated test", updatedTask.getName());
            Assert.assertEquals("updated test description", updatedTask.getDescription());
        }
    }

    @Test
    public void updateByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.updateByIndex(
                    user.getId(), task.getId(), "updated test", "updated test description"
            );
            @Nullable final Task updatedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals("updated test", updatedTask.getName());
            Assert.assertEquals("updated test description", updatedTask.getDescription());
        }
    }

    @Test
    public void changeStatusByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.updateStatusByName(user.getId(), task.getName(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Task statusChangedTask = taskRepository.findByName(user.getId(), task.getName());
            Assert.assertNotNull(statusChangedTask);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());

        }
    }

    @Test
    public void changeStatusById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.updateStatusById(user.getId(), task.getId(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Task statusChangedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(statusChangedTask);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void changeStatusByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.updateStatusByIndex(user.getId(), task.getId(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Task statusChangedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(statusChangedTask);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.startByName(user.getId(), task.getName(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            @Nullable final Task startedTask = taskRepository.findByName(user.getId(), task.getName());
            Assert.assertNotNull(startedTask);
            Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.startById(user.getId(), task.getId(), Status.IN_PROGRESS.getDisplayName());
            @Nullable final Task startedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            session.commit();
            Assert.assertNotNull(startedTask);
            Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.startByIndex(user.getId(), task.getId(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            @Nullable final Task startedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(startedTask);
            Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.finishByName(user.getId(), task.getName(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Task finishedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(finishedTask);
            Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.finishById(user.getId(), task.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Task finishedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(finishedTask);
            Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.finishByIndex(user.getId(), task.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Task finishedTask = taskRepository.findByUserIdAndTaskId(user.getId(), task.getId());
            Assert.assertNotNull(finishedTask);
            Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
            taskRepository.updateStatusByName(user.getId(), task.getName(), task.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void clear() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            @Nullable List<Task> tasks = taskRepository.findAll();
            Assert.assertNotNull(tasks);
            taskRepository.clear();
            session.commit();
            Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAll());
            tasks.forEach(taskRepository::add);
            session.commit();
        }
    }

    @Test
    public void removeByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.removeByName(user.getId(), task.getName());
            session.commit();
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            taskRepository.add(task);
            session.commit();
        }
    }

    @Test
    public void removeById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.removeById(task.getId());
            session.commit();
            Assert.assertNull(taskRepository.findById(task.getId()));
            taskRepository.add(task);
            session.commit();
        }
    }

    @Test
    public void findTaskByProjectId() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            @Nullable final List<Task> tasks = taskRepository.findTaskByProjectId(user.getId(), project.getId());
            Assert.assertNotNull(tasks);
            tasks.forEach(t -> Assert.assertEquals(project.getId(), t.getProjectId()));
        }
    }

    @Test
    public void unbindTaskById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.unbindTaskById(user.getId(), task.getId());
            session.commit();
            @Nullable final Task testTask = taskRepository.findById(task.getId());
            Assert.assertNotNull(testTask);
            Assert.assertNull(testTask.getProjectId());
        }
    }

    @Test
    public void bindTaskById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(task);
            taskRepository.bindTaskById(user.getId(), task.getId(), task.getId());
            session.commit();
            @Nullable final Task testTask = taskRepository.findById(task.getId());
            Assert.assertNotNull(testTask);
            Assert.assertNull(testTask.getProjectId());
        }
    }

}