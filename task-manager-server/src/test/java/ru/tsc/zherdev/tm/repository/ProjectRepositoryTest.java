package ru.tsc.zherdev.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private Project project;

    @Nullable
    private User user;

    @NotNull
    public IProjectRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IProjectRepository.class);
    }

    @Before
    public void before() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            user = new User("login", "passwordHash");
            Assert.assertNotNull(user);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());
            Assert.assertNotNull(user);

            projectRepository.add(new Project(user.getId(), "test", "test description"));
            project = projectRepository.findByName(user.getId(), "test");
            Assert.assertNotNull(project);
            Assert.assertNotNull(project.getId());
            Assert.assertNotNull(project.getName());
            Assert.assertEquals("test", project.getName());
        }
    }

    @Test
    public void add() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            if (project == null) Assert.fail();
            @Nullable final Project projectById = projectRepository.findById(project.getId());
            Assert.assertNotNull(projectById);
            Assert.assertEquals(project, projectById);
        }
    }

    @Test
    public void addAll() {
        @Nullable List<Project> projects = new ArrayList<>();
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            if (user == null) Assert.fail();
            projects.add(new Project(user.getId(), "test2", "test2 description"));
            projects.add(new Project(user.getId(), "test3", "test3 description"));
            projects.forEach(projectRepository::add);
            projects = projectRepository.findAll();
            Assert.assertNotEquals(Collections.EMPTY_LIST, projects);
            Assert.assertNotNull(projects);
            Assert.assertEquals(2, projects.size());
        }
    }

    @Test
    public void findAll() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable List<Project> projects = projectRepository.findAll();
            Assert.assertNotNull(projects);
            Assert.assertEquals(1, projects.size());
        }
    }

    @Test
    public void findAllByUserId() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable List<Project> projects = projectRepository.findAllByUserId(user.getId());
            Assert.assertNotNull(projects);
            Assert.assertEquals(1, projects.size());
        }
    }

    @Test
    public void findByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            @Nullable final Project projectByName = projectRepository.findByName(user.getId(), project.getName());
            Assert.assertNotNull(projectByName);
        }
    }

    @Test
    public void findById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            @Nullable final Project projectById = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(projectById);
        }
    }

    @Test
    public void findByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final Project projectByIndex = projectRepository.findByIndex(user.getId(), 0);
            Assert.assertNotNull(projectByIndex);
        }
    }

    @Test
    public void existsByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final Project projectByIndex = projectRepository.findByIndex(user.getId(), 0);
            Assert.assertNotNull(projectByIndex);
        }
    }

    @Test
    public void existsById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            @Nullable final Project projectById = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(projectById);
        }
    }

    @Test
    public void updateById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.updateById(
                    user.getId(), project.getId(), "updated test", "updated test description"
            );
            session.commit();
            @Nullable final Project updatedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals("updated test", updatedProject.getName());
            Assert.assertEquals("updated test description", updatedProject.getDescription());
        }
    }

    @Test
    public void updateByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.updateByIndex(
                    user.getId(), project.getId(), "updated test", "updated test description"
            );
            @Nullable final Project updatedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals("updated test", updatedProject.getName());
            Assert.assertEquals("updated test description", updatedProject.getDescription());
        }
    }

    @Test
    public void changeStatusByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.updateStatusByName(user.getId(), project.getName(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Project statusChangedProject = projectRepository.findByName(user.getId(), project.getName());
            Assert.assertNotNull(statusChangedProject);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());

        }
    }

    @Test
    public void changeStatusById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.updateStatusById(user.getId(), project.getId(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Project statusChangedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(statusChangedProject);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void changeStatusByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.updateStatusByIndex(user.getId(), project.getId(), Status.IN_PROGRESS.toString());
            session.commit();
            @Nullable final Project statusChangedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(statusChangedProject);
            Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.startByName(user.getId(), project.getName(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            @Nullable final Project startedProject = projectRepository.findByName(user.getId(), project.getName());
            Assert.assertNotNull(startedProject);
            Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.startById(user.getId(), project.getId(), Status.IN_PROGRESS.getDisplayName());
            @Nullable final Project startedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            session.commit();
            Assert.assertNotNull(startedProject);
            Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void startByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.startByIndex(user.getId(), project.getId(), Status.IN_PROGRESS.getDisplayName());
            session.commit();
            @Nullable final Project startedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(startedProject);
            Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.finishByName(user.getId(), project.getName(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Project finishedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(finishedProject);
            Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.finishById(user.getId(), project.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Project finishedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(finishedProject);
            Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void finishByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.finishByIndex(user.getId(), project.getId(), Status.COMPLETE.getDisplayName());
            session.commit();
            @Nullable final Project finishedProject = projectRepository.findByUserIdAndProjectId(user.getId(), project.getId());
            Assert.assertNotNull(finishedProject);
            Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
            projectRepository.updateStatusByName(user.getId(), project.getName(), project.getStatus().toString());
            session.commit();
        }
    }

    @Test
    public void clear() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            @Nullable List<Project> projects = projectRepository.findAll();
            Assert.assertNotNull(projects);
            projectRepository.clear();
            session.commit();
            Assert.assertEquals(Collections.EMPTY_LIST, projectRepository.findAll());
            projects.forEach(projectRepository::add);
            session.commit();
        }
    }

    @Test
    public void removeByName() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.removeByName(user.getId(), project.getName());
            session.commit();
            Assert.assertNotNull(projectRepository.findById(project.getId()));
            projectRepository.add(project);
            session.commit();
        }
    }

    @Test
    public void removeById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            Assert.assertNotNull(user);
            Assert.assertNotNull(project);
            projectRepository.removeById(project.getId());
            session.commit();
            Assert.assertNull(projectRepository.findById(project.getId()));
            projectRepository.add(project);
            session.commit();
        }
    }

}