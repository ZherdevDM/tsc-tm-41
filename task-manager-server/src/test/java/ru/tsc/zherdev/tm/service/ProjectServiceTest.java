package ru.tsc.zherdev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private ProjectService projectService;

    @Nullable
    private Project project;

    @Nullable
    private User user;

    @Before
    public void before() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            projectService = new ProjectService(dataConnectionService, logService);

            user = new User("login", "passwordHash");
            Assert.assertNotNull(user);
            user.setRole(Role.ADMIN);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());

            projectRepository.add(new Project(user.getId(), "test", "test description"));
            project = projectRepository.findByName(user.getId(), "test");
            Assert.assertNotNull(project);
            Assert.assertNotNull(project.getId());
            Assert.assertNotNull(project.getName());
            Assert.assertEquals("test", project.getName());
        }
    }

    @Test
    public void add() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectById = projectService.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void addAll() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull List<Project> projects = new ArrayList<>();
        projects.add(new Project(user.getId(), "test2", "test2 description"));
        projects.add(new Project(user.getId(), "test3", "test3 description"));
        projectService.addAll(projects);
        projects = projectService.findAll(user.getId());
        Assert.assertNotEquals(Collections.EMPTY_LIST, projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findAll() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findByName() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectByName = projectService.findByName(user.getId(), project.getName());
        Assert.assertNotNull(projectByName);
    }

    @Test
    public void findById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectById = projectService.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectByIndex = projectService.findByIndex(user.getId(), 0);
        Assert.assertNotNull(projectByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        int projectSize = projectService.getSize(user.getId());
        Assert.assertEquals(projectSize, projects.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectByIndex = projectService.findByIndex(user.getId(), 0);
        Assert.assertNotNull(projectByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project projectById = projectService.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void updateById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project updatedProject = projectService.updateById(
                user.getId(), project.getId(), "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("updated test", updatedProject.getName());
        Assert.assertEquals("updated test description", updatedProject.getDescription());
    }

    @Test
    public void updateByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project updatedProject = projectService.updateByIndex(
                user.getId(), 0, "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("updated test", updatedProject.getName());
        Assert.assertEquals("updated test description", updatedProject.getDescription());
    }

    @Test
    public void changeStatusByName() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project statusChangedProject = projectService.changeStatusByName(user.getId(), project.getName(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void changeStatusById() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project statusChangedProject = projectService.changeStatusById(user.getId(), project.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project statusChangedProject =
                projectService.changeStatusByIndex(user.getId(), 0, Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startByName() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project startedProject = projectService.startByName(user.getId(), project.getName());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project startedProject = projectService.startById(user.getId(), project.getId());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project startedProject = projectService.startByIndex(user.getId(), 0);
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishByName() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project finishedProject = projectService.finishByName(user.getId(), project.getName());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project finishedProject = projectService.finishById(user.getId(), project.getId());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project finishedProject = projectService.finishByIndex(user.getId(), 0);
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectService.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void clear() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        projectService.clear(user.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, projectService.findAll(user.getId()));
        projectService.addAll(projects);
    }

    @Test
    public void remove() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        projectService.remove(user.getId(), project);
        Assert.assertNull(projectService.findById(user.getId(), project.getId()));
        projectService.add(user.getId(), project);
    }

    @Test
    public void removeByName() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        projectService.removeByName(user.getId(), project.getName());
        Assert.assertNull(projectService.findById(user.getId(), project.getId()));
        projectService.add(user.getId(), project);
    }

    @Test
    public void removeById() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        projectService.removeById(user.getId(), project.getId());
        Assert.assertNull(projectService.findById(user.getId(), project.getId()));
        projectService.add(user.getId(), project);
    }

    @Test
    public void removeByIndex() throws SQLException {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @Nullable final Project removableProject = projectService.add(
                user.getId(), new Project(user.getId(), "remove test", "remove test description")
        );
        Assert.assertNotNull(removableProject);
        int projectsSize = projectService.findAll(user.getId()).size();
        Assert.assertNotEquals(0, projectsSize);
        projectService.removeByIndex(user.getId(), projectsSize - 1);
        Assert.assertNull(projectService.findById(user.getId(), removableProject.getId()));
    }

}
