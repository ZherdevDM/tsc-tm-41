package ru.tsc.zherdev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.exception.user.EmptyUserException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private SessionService sessionService;

    @NotNull
    private DataConnectionService dataConnectionService;

    @NotNull
    private Session session;

    @Nullable
    private User user;

    @Before
    public void before() {
        @NotNull final ILogService logService = new LogService();
        @NotNull final PropertyService propertyService = new PropertyService();
        dataConnectionService = new DataConnectionService(propertyService, logService);
        try (@NotNull final SqlSession userSession = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = userSession.getMapper(IUserRepository.class);
            @NotNull final UserService userService = new UserService(dataConnectionService, propertyService, logService);
            sessionService = new SessionService(logService, dataConnectionService, userService, propertyService);
            user = new User("login", "dd554650e5558cbe375c6bf958741161", Role.ADMIN);
            Assert.assertNotNull(user);
            userRepository.add(user);
            user = userRepository.findUserByLogin(user.getLogin());
            Assert.assertNotNull(user);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());

            session = new Session();
            session.setUserId(user.getId());
            Assert.assertNotNull(session);
            sessionService.sign(session);
            Assert.assertNotNull(session.getId());
            sessionService.open(session);
        }
    }

    @Test
    public void add() {
        sessionService.open(session);
        Assert.assertTrue(sessionService.isValid(session));
    }

    @Test
    public void findAllBySessionId() throws SQLException {
        @Nullable List<Session> sessions = sessionService.getListSession(session);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(1, sessions.size());
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Session sessionById = sessionService.getSessionById(session, session.getId());
        Assert.assertNotNull(sessionById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Session sessionByIndex = sessionService.getSessionByIndex(session, 0);
        Assert.assertNotNull(sessionByIndex);
    }

    @Test
    public void remove() throws SQLException {
        @NotNull final Session closeableSession = new Session();
        sessionService.close(closeableSession);
        Assert.assertNull(sessionService.getSessionById(session, closeableSession.getId()));
    }

    @Test
    public void open() throws SQLException {
        @NotNull final Session testSession = new Session();
        sessionService.open(testSession);
        @Nullable Session openedSession = sessionService.getSessionById(session, testSession.getId());
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(session.getId(), openedSession.getId());
    }

    @Test
    public void existsById() {
        @Nullable Session nonContainedSession = new Session();
        Assert.assertNotNull(nonContainedSession);
        Assert.assertFalse(sessionService.isValid(nonContainedSession));
        Assert.assertTrue(sessionService.isValid(session));
    }

    @Test
    public void close() throws SQLException {
        @NotNull final User newUser = createNewUser();
        @Nullable final Session closableSession = createNewSession(newUser);
        Assert.assertNotNull(closableSession);
        @Nullable final Session openedSession = sessionService.open(closableSession);
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(closableSession.getId(), openedSession.getId());
        sessionService.close(closableSession);
        Assert.assertNull(sessionService.getSessionById(session, closableSession.getId()));
        removeUserAndSession(newUser, null);
    }

    @Test
    public void checkDataAccess() throws SQLException {
        Assert.assertNotNull(user);
        boolean isValid = sessionService.checkDataAccess(user.getLogin(), "admin");
        Assert.assertTrue(isValid);
        isValid = sessionService.checkDataAccess("wrong login", "Not a passwordHash");
        Assert.assertFalse(isValid);
    }

    @Test
    public void login() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signAndValidate() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        sessionService.sign(newSession);
        try {
            sessionService.validate(newSession);
        } catch (AccessDeniedException e) {
            Assert.fail();
        }

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void getUser() throws SQLException {
        Assert.assertNotNull(user);
        @Nullable final User accessedUser = sessionService.getUser(session);
        Assert.assertNotNull(accessedUser);
        Assert.assertEquals(user.getId(), accessedUser.getId());
    }

    @Test
    public void getUserId() {
        Assert.assertNotNull(user);
        @Nullable final String accessedUserId = sessionService.getUserId(session);
        Assert.assertNotNull(accessedUserId);
        Assert.assertEquals(user.getId(), accessedUserId);
    }

    @Test
    public void getListSession() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        @Nullable List<Session> sessions = sessionService.getListSession(newSession);
        Assert.assertNotNull(sessions);

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void closeAll() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        Assert.assertNotEquals(Collections.EMPTY_LIST, sessionService.getListSession(session));
        sessionService.closeAll(session);
        Assert.assertEquals(Collections.EMPTY_LIST, sessionService.getListSession(session));

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void validateAdmin() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        try {
            sessionService.validateAdmin(newSession);
        } catch (AccessDeniedException | SQLException e) {
            Assert.fail();
        }
        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void isValid() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        boolean isValid = sessionService.isValid(newSession);
        Assert.assertTrue(isValid);
        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signOutByLogin() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        sessionService.signOutByLogin(newUser.getLogin());
        Assert.assertNull(sessionService.getSessionById(session, newSession.getId()));

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signOutByUserId() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        sessionService.signOutByUserId(newUser.getId());
        Assert.assertNull(sessionService.getSessionById(session, newSession.getId()));

        removeUserAndSession(newUser, newSession);

    }

    @NotNull
    private User createNewUser() {
        try (@NotNull final SqlSession userSession = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = userSession.getMapper(IUserRepository.class);
            userRepository.add(new User("user", "78a71b2088b00afe56d5b90e22f805fa"));
            userSession.commit();
            @Nullable User newUser = userRepository.findUserByLogin("user");
            Assert.assertNotNull(newUser);
            newUser.setRole(Role.ADMIN);
            userRepository.add(newUser);
            return newUser;
        }
    }

    @NotNull
    private Session createNewSession(@NotNull final User newUser) throws SQLException {
        @Nullable Session newSession = sessionService.login(newUser.getLogin(), "user");
        Assert.assertNotNull(newSession);
        Assert.assertNotNull(sessionService.getSessionById(session, newSession.getId()));
        return newSession;
    }

    private void removeUserAndSession(@Nullable final User newUser, @Nullable final Session newSession) {
        try (@NotNull final SqlSession userSession = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = userSession.getMapper(IUserRepository.class);
            if (newUser == null) throw new EmptyUserException();
            if (user != null) userRepository.remove(newUser);
            if (newSession != null) sessionService.close(newSession);
        }
    }

}
