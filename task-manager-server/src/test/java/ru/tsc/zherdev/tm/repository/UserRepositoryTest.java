package ru.tsc.zherdev.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.util.Collections;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private User user;

    @NotNull
    public IUserRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IUserRepository.class);
    }

    @Before
    public void before() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(new User("login", "passwordHash"));
            session.commit();
            user = userRepository.findUserByLogin("login");
            Assert.assertNotNull(user);
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getLogin());
            Assert.assertEquals("login", user.getLogin());
        }
    }

    @Test
    public void add() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final User userById = userRepository.findById(user.getId());
            Assert.assertNotNull(userById);
            Assert.assertEquals(user, userById);
        }
    }

    @Test
    public void findAll() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable List<User> users = userRepository.findAll();
            Assert.assertNotNull(users);
            Assert.assertEquals(1, users.size());
        }
    }

    @Test
    public void clear() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable List<User> users = userRepository.findAll();
            Assert.assertNotNull(users);
            userRepository.clear();
            session.commit();
            Assert.assertEquals(Collections.EMPTY_LIST, userRepository.findAll());
            users.forEach(userRepository::add);
            session.commit();
        }
    }

    @Test
    public void removeById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.removeUserById(user.getId());
            session.commit();
            Assert.assertNull(userRepository.findById(user.getId()));
            userRepository.add(user);
            session.commit();
        }
    }

    @Test
    public void findById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable final User userById = userRepository.findById(user.getId());
            Assert.assertNotNull(userById);
        }
    }

    @Test
    public void findByIndex() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User userByIndex = userRepository.findByIndex(0);
            Assert.assertNotNull(userByIndex);
        }
    }

    @Test
    public void findUserByLogin() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable User userByLogin = userRepository.findUserByLogin(user.getLogin());
            Assert.assertNotNull(userByLogin);
            Assert.assertEquals(user, userByLogin);
        }
    }

    @Test
    public void findUserByEmail() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            @Nullable User userByEmail = userRepository.findUserByEmail(user.getEmail());
            Assert.assertNotNull(userByEmail);
            Assert.assertEquals(user, userByEmail);
        }
    }

    @Test
    public void removeUserById() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.removeUserById(user.getId());
            session.commit();
            Assert.assertNull(userRepository.findById(user.getId()));
            userRepository.add(user);
            session.commit();
        }
    }

    @Test
    public void removeUserByLogin() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.removeUserByLogin(user.getLogin());
            session.commit();
            Assert.assertNull(userRepository.findUserByLogin(user.getLogin()));
            userRepository.add(user);
            session.commit();
        }
    }

    @Test
    public void update() {
        @NotNull final String email = "email@g.com";
        @NotNull final String lastName = "Smith";
        @NotNull final String firstName = "John";
        @NotNull final String middleName = "Billy";
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.update(user.getId(), user.getLogin(),
                    user.getPasswordHash(), email, Role.ADMIN, lastName, firstName, middleName);
            session.commit();
            @Nullable User updatedUser = userRepository.findUserByEmail(email);
            Assert.assertNotNull(updatedUser);
            Assert.assertEquals(user.getId(), updatedUser.getId());
            Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
            Assert.assertEquals(user.getPasswordHash(), updatedUser.getPasswordHash());
            Assert.assertEquals(email, updatedUser.getEmail());
            Assert.assertEquals(Role.ADMIN, updatedUser.getRole());
            Assert.assertEquals(lastName, updatedUser.getLastName());
            Assert.assertEquals(firstName, updatedUser.getFirstName());
            Assert.assertEquals(middleName, updatedUser.getMiddleName());
        }
    }

    @Test
    public void lockUserByLogin() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.lockUserByLogin(user.getLogin());
            session.commit();
            @Nullable User userByLogin = userRepository.findUserByLogin(user.getLogin());
            Assert.assertNotNull(userByLogin);
            Assert.assertEquals(true, userByLogin.getLocked());
            userRepository.unLockUserByLogin(user.getLogin());
        }
    }

    @Test
    public void unLockUserByLogin() {
        try (@NotNull final SqlSession session = dataConnectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            Assert.assertNotNull(user);
            userRepository.unLockUserByLogin(user.getLogin());
            session.commit();
            @Nullable User userByLogin = userRepository.findUserByLogin(user.getLogin());
            Assert.assertNotNull(userByLogin);
            Assert.assertEquals(false, userByLogin.getLocked());
        }
    }

}
