package ru.tsc.zherdev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.Task;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskShowByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.TASK_SHOW_BY_ID;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() throws SQLException_Exception {
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = toolsLocator.getTaskEndpoint().findByIdTask(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println(task);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}
