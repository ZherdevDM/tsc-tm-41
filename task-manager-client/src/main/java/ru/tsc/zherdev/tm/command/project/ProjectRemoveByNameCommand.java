package ru.tsc.zherdev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Project;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_NAME;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Project project = toolsLocator.getProjectEndpoint().findByNameProject(session, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        toolsLocator.getProjectTaskEndpoint().removeProjectById(session, project.getId());
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}