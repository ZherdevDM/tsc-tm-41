package ru.tsc.zherdev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

public final class AboutDisplayCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.ABOUT;
    }

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[About]");
        System.out.println("DEVELOPER: " + toolsLocator.getPropertyService().getDeveloperName());
        System.out.println("EMAIL: " + toolsLocator.getPropertyService().getDeveloperEmail());
    }
}