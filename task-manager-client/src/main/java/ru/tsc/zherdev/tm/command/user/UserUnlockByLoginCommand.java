package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.empty.EmptyLoginException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Unlock User by login. Administrative command.";
    }

    @Override
    public void execute() throws SQLException_Exception {
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[UNLOCK USER BY LOGIN]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = toolsLocator.getAdminUserEndpoint().findUserByLoginUser(session, login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        toolsLocator.getAdminUserEndpoint().unLockUserByLoginUser(session, login);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
