package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error. '" + value + "' is not number.");
    }

}
