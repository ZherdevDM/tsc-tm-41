package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    final static String USE_HELP = "Command is unknown. Use '" + TerminalConst.HELP + "' for display available commands.";

    public UnknownCommandException() {
        super(USE_HELP);
    }

    public UnknownCommandException(String command) {
        super("No such command as '" + command + "'. " + USE_HELP);
    }
}
