package ru.tsc.zherdev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.zherdev.tm.api.repository.ICommandRepository;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.service.*;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.comparator.CommandComparator;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.entity.InvalidSessionException;
import ru.tsc.zherdev.tm.exception.system.UnknownCommandException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.repository.CommandRepository;
import ru.tsc.zherdev.tm.repository.SessionRepository;
import ru.tsc.zherdev.tm.service.CommandService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;
import ru.tsc.zherdev.tm.service.SessionService;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.lang.Exception;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Bootstrap implements IToolsLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IToolsLocator toolsLocator = (IToolsLocator) this;
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private final ISessionService sessionService = new SessionService(toolsLocator, sessionRepository);
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
    @NotNull
    private final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @Nullable
    private Session session = null;

    public Session getSession() {
        if (sessionEndpoint.isValidSession(session))
            return session;
        else throw new InvalidSessionException();
    }

    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    @Nullable
    public String getUserId() {
        if (session == null) throw new AccessDeniedException();
        return session.getUserId();
    }

    public void start(@NotNull String... args) throws SQLException_Exception {
        displayWelcome();
        initCommands();
        if (args.length != 0) parseArgs(args);
        process();
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.zherdev.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.zherdev.tm.command.AbstractCommand.class)
                .stream()
                .sorted(CommandComparator.getInstance())
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registerCommand(clazz.newInstance());
        }
    }

    public void runCommand(@NotNull final String command) throws SQLException_Exception {
        if (command.isEmpty()) return;
        @NotNull final AbstractCommand appliedCommand = commandService.getCommandByName(command);
        @Nullable List<Role> roles = null;
        if (appliedCommand.roles() != null) roles = Arrays.asList(appliedCommand.roles());
        appliedCommand.execute();
    }

    public void process() {
        logService.debug("Test environment.");
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {

            @NotNull final String terminalCommand = command.name();
            @Nullable final String commandDescription = command.description();
            @Nullable final String commandArgument = command.arg();
            if (terminalCommand.isEmpty()) throw new EmptyNameException();
            if (commandDescription.isEmpty()) throw new EmptyDescriptionException();
            command.setToolsLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (commandArgument == null || commandArgument.isEmpty()) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull AbstractException e) {
            logService.error(e);
        }
    }

    public void parseArgs(@NotNull final String[] args) throws SQLException_Exception {
        if (args.length == 0) return;
        final String arg = args[0];
        runArgument(arg);
        System.exit(0);
    }

    private void runArgument(@NotNull String arg) throws SQLException_Exception {
        if (arg.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByArg(arg);
        if (abstractCommand == null) throw new UnknownCommandException(arg);
        abstractCommand.execute();
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER CLIENT **");
    }

}
