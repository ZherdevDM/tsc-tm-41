package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyEntityException extends AbstractException {

    public EmptyEntityException() {
        super("Error. Entity is empty.");
    }

}
