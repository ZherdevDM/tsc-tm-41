package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
