
package ru.tsc.zherdev.tm.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for sort.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="sort"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NAME"/&amp;gt;
 *     &amp;lt;enumeration value="CREATED"/&amp;gt;
 *     &amp;lt;enumeration value="START_DATE"/&amp;gt;
 *     &amp;lt;enumeration value="STATUS"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "sort")
@XmlEnum
public enum Sort {

    NAME,
    CREATED,
    START_DATE,
    STATUS;

    public String value() {
        return name();
    }

    public static Sort fromValue(String v) {
        return valueOf(v);
    }

}
