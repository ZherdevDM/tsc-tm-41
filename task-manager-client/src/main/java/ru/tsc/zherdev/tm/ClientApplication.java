package ru.tsc.zherdev.tm;

import lombok.SneakyThrows;
import ru.tsc.zherdev.tm.component.Bootstrap;

public class ClientApplication {

    @SneakyThrows
    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
