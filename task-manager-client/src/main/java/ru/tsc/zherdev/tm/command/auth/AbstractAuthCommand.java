package ru.tsc.zherdev.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;

public abstract class AbstractAuthCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        if (!(user.getLastName() == null) && !user.getLastName().isEmpty())
            System.out.println("Full Name: " + user.getLastName() + " " +
                    user.getFirstName() + " " + user.getMiddleName());
        if (!(user.getEmail() == null) && !user.getEmail().isEmpty())
            System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole());
    }

}
