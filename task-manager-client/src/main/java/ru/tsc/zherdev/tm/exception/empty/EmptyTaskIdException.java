package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyTaskIdException extends AbstractException {
    public EmptyTaskIdException() {
        super("Error. Task ID is empty.");
    }
}
