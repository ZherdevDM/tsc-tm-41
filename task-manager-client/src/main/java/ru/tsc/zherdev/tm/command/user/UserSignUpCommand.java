package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserSignUpCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.SIGN_UP;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Create new user profile.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[CREATE NEW USER]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final User user = toolsLocator.getAdminUserEndpoint().createUserExtraEmail(session, login, password, email);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
