package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.*;

public interface IToolsLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

}
