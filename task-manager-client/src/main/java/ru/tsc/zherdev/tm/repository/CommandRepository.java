package ru.tsc.zherdev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ICommandRepository;
import ru.tsc.zherdev.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final HashMap<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final HashMap<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @NotNull
    public HashMap<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    @NotNull
    public HashMap<String, AbstractCommand> getArguments() {
        return arguments;
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        return commands.values().stream()
                .filter((o -> o.name() == null || o.name().isEmpty()))
                .map(AbstractCommand::name)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public Collection<String> getCommandArg() {
        return arguments.values().stream()
                .filter((o -> o.name() == null || o.name().isEmpty()))
                .map(AbstractCommand::name)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        @NotNull final String name = command.name();
        @Nullable final String arg = command.arg();
        commands.put(name, command);
        if (arg != null) arguments.put(arg, command);
    }

}
