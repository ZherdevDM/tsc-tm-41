package ru.tsc.zherdev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

import java.util.HashMap;

public final class CommandsDisplayCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.COMMANDS;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final HashMap<String, AbstractCommand> commands = toolsLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands.values()) showCommandValue(command.name());
    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        final String bullet = "• ";
        System.out.println(bullet + value);
    }

}