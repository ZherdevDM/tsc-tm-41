package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.constant.ArgumentConst;
import ru.tsc.zherdev.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Argument is unknown. Use '" + ArgumentConst.HELP + "' for display available arguments.");
    }

}
