package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.USER_REMOVE_BY_ID;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() throws SQLException_Exception {
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[REMOVE USER]");
        System.out.println("Enter ID:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @NotNull final Result result = toolsLocator.getAdminUserEndpoint().removeUserByIdUser(session, userId);
        if (result.isSuccess()) System.out.println("[SUCCESS]");
        if (!result.isSuccess()) {
            System.out.println("[FAILED]");
            System.out.println(result.getMessage());
        }
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
